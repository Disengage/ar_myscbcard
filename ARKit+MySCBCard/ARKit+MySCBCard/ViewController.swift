//
//  ViewController.swift
//  ARKit+MySCBCard
//
//  Created by Narongsak Kongpan on 18/7/2563 BE.
//  Copyright © 2563 Narongsak Kongpan. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {
  
  @IBOutlet private weak var sceneView: ARSCNView!
  @IBOutlet private weak var btnTest: UIButton!
  
  private var scbLogoNode: SCNNode? {
    if let scene: SCNScene = SCNScene(named: "art.scnassets/SCB_logo.scn") {
      let logoNode = scene.rootNode
      logoNode.name = "logo"
      return logoNode
    }
    return nil
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Set the view's delegate
    sceneView.delegate = self
    
    // Show statistics such as fps and timing information
    sceneView.showsStatistics = true
    
    sceneView.autoenablesDefaultLighting = true
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    // Create a session configuration
    let configuration = ARImageTrackingConfiguration()
    if let trackingImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: Bundle.main) {
      configuration.trackingImages = trackingImages
      configuration.maximumNumberOfTrackedImages = 1
    }
    
    // Run the view's session
    sceneView.session.run(configuration)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    // Pause the view's session
    sceneView.session.pause()
  }
  
  // MARK: - ARSCNViewDelegate
  
  // Override to create and configure nodes for anchors added to the view's session.
  func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
    if let imageAnchor = anchor as? ARImageAnchor {
      var node = SCNNode()
      node.name = "mainNode"
      
      // Read reference image size
      let size = imageAnchor.referenceImage.physicalSize
      
      // Create plate with purple color's background
      let plane = SCNPlane(width: size.width, height: size.height)
      plane.firstMaterial?.diffuse.contents = "#4D2B83".toUIColor()
      plane.cornerRadius = 0.3
      
      // Make plate node & rotate X by 90 degree
      let planeNode = SCNNode(geometry: plane)
      planeNode.eulerAngles.x = -.pi / 2
      node.addChildNode(planeNode)
      
      // Add additional node
      displaySCBLogo(node: &node)
      displayCardInfo(imageAnchor: imageAnchor, node: &node)
      
      return node
    }
    return nil
  }
  
  private var scaleAnimate: CABasicAnimation?
  
  func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
    if let imageAnchor = anchor as? ARImageAnchor {
      // print("\(imageAnchor.referenceImage.name ?? "") did update \(node): \(anchor)")
      print("\(imageAnchor.referenceImage.name ?? "") is track ? \(imageAnchor.isTracked)")
      if imageAnchor.isTracked {
        addLogoAnimation(node: node)
      } else {
        removeAllAnimation(node: node)
      }
    }
  }
  
  func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
    if let imageAnchor = anchor as? ARImageAnchor {
      print("\(imageAnchor.referenceImage.name ?? "") did add \(node): \(anchor)")
    }
  }
  
  func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
    if let imageAnchor = anchor as? ARImageAnchor {
      print("\(imageAnchor.referenceImage.name ?? "") did removed \(node): \(anchor)")
    }
  }
  
  func session(_ session: ARSession, didFailWithError error: Error) {
    // Present an error message to the user
    
  }
  
  func sessionWasInterrupted(_ session: ARSession) {
    // Inform the user that the session has been interrupted, for example, by presenting an overlay
    
  }
  
  func sessionInterruptionEnded(_ session: ARSession) {
    // Reset tracking and/or remove existing anchors if consistent tracking is required
    
  }
  
  @IBAction func didTapBtnTest(_ sender: Any) {
    removeAllAnimation(node: sceneView.scene.rootNode)
    addLogoAnimation(node: sceneView.scene.rootNode)
  }
}

extension ViewController {
  func removeAllAnimation(node: SCNNode) {
    node.childNodes.forEach({ $0.removeAllAnimations() })
    scaleAnimate = nil
  }
  
  func addLogoAnimation(node: SCNNode) {
    guard let logoNode = node.childNodes.first(where: { $0.name == "logo" }), scaleAnimate == nil else { return }
    scaleAnimate = CABasicAnimation(keyPath: "transform")
    scaleAnimate?.duration = 0.5
    scaleAnimate?.isRemovedOnCompletion = false
    scaleAnimate?.fillMode = .forwards
    scaleAnimate?.timingFunction = .init(name: .easeIn)
    scaleAnimate?.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
    scaleAnimate?.toValue = NSValue(caTransform3D: CATransform3DMakeScale(3, 3, 3))
    logoNode.addAnimation(SCNAnimation(caAnimation: scaleAnimate!), forKey: "scale")
  }
}

extension ViewController {
  func displaySCBLogo(node: UnsafePointer<SCNNode>) {
    guard let scbLogo = scbLogoNode else { return }
    
    // Add logo
    //scbLogo.scale = SCNVector3(3, 3, 3)
    scbLogo.position = SCNVector3(0, 0, 0.5)
    
    //let scale = SCNAction.scale(by: 3, duration: 0.5)
    //scale.timingMode = .easeIn
    //scbLogo.runAction(scale, forKey: "scale")
    
    // Add logo label
    let label = SCNText(string: "SCB", extrusionDepth: 0.1)
    label.font = UIFont(name: "SukhumvitSet-SemiBold", size: 1.25)
    label.flatness = 0
    
    // Add logo material
    let textMaterial = SCNMaterial()
    textMaterial.diffuse.contents = UIColor.white
    label.materials = [textMaterial]
    
    // Make logo lebel node
    let labelNode = SCNNode()
    labelNode.geometry = label
    
    // Adjust logo label position
    let (_, boundingBoxMax) = labelNode.boundingBox
    labelNode.position = SCNVector3(-boundingBoxMax.x / 2, 0, boundingBoxMax.y * 1.5)
    
    // Rotate logo label
    let rotate = SCNAction.rotateBy(x: -.pi / 2, y: 0, z: 0, duration: 0.5)
    labelNode.runAction(rotate, forKey: "rotate")
    
    // Add logo node to root
    node.pointee.addChildNode(scbLogo)
    node.pointee.addChildNode(labelNode)
  }
  
  func displayCardInfo(imageAnchor: ARImageAnchor, node: UnsafePointer<SCNNode>) {
    guard imageAnchor.referenceImage.name == "s88009" else { return }
    let position = SCNVector3ToGLKVector3(node.pointee.position)
    print("position: \(position.x), \(position.y), \(position.z)")
  }
}
